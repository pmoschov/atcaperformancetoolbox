#!/bin/bash

# SNMP settings
SNMP_HOST="192.168.0.2"
SNMP_COMMUNITY="public"
SNMP_VERSION="2c"
ROOT_OID="1.3.6.1.4.1.16394.2.1.1"

# Default values
IPMB_ADDR="130" # 0x82 in decimal
INITIAL_SENSOR_ADDR=14
NUM_SENSORS=123
VERBOSE=0

usage() {
    echo "Usage: $0 [-v] [-i INITIAL_SENSOR_ADDR] [-n NUM_SENSORS]"
    echo "Options:"
    echo "  -v                      Enable verbose output."
    echo "  -i INITIAL_SENSOR_ADDR  Set the initial sensor address (default is 14)."
    echo "  -n NUM_SENSORS          Set the number of sensors to read (default is 123)."
    echo "  -h                      Display this help message."
}

while getopts "hvi:n:" opt; do
    case $opt in
        v)
            VERBOSE=1
            ;;
        i)
            INITIAL_SENSOR_ADDR=$OPTARG
            ;;
        n)
            NUM_SENSORS=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1
            ;;
    esac
done

fetch_snmp_data() {
    local oid=$1
    local cmd="snmpget -v $SNMP_VERSION -c $SNMP_COMMUNITY $SNMP_HOST $oid"

    local output=$($cmd | awk '{print $4}')

    if [ $VERBOSE -eq 1 ]; then
        echo -e "\nSNMP Request: $cmd" >&2
        echo "SNMP Reply: $output" >&2
    fi

    echo $output
}

for ((i=$INITIAL_SENSOR_ADDR; i<INITIAL_SENSOR_ADDR+NUM_SENSORS; i++)); do

    sensor_name=$(fetch_snmp_data "$ROOT_OID.3.1.43.$IPMB_ADDR.$i")
    sensor_reading=$(fetch_snmp_data "$ROOT_OID.3.1.28.$IPMB_ADDR.$i")
    sensor_unit_index=$(fetch_snmp_data "$ROOT_OID.3.1.17.$IPMB_ADDR.$i")
    if [ "$sensor_unit_index" == "1" ]; then
        sensor_unit="degrees C" # Celsius for temperature
    else
        sensor_unit="unknown"
    fi

    echo "$sensor_name | $sensor_reading $sensor_unit"
done
