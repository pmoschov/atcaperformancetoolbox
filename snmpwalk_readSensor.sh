#!/bin/bash

# SNMP settings
SNMP_HOST="192.168.0.2"
SNMP_COMMUNITY="public"
SNMP_VERSION="2c"
ROOT_OID="1.3.6.1.4.1.16394.2.1.1"

# Default values
IPMB_ADDR="130" # 0x82 in decimal
INITIAL_SENSOR_ADDR=14
NUM_SENSORS=123
VERBOSE=0

usage() {
    echo "Usage: $0 [-v] [-i INITIAL_SENSOR_ADDR] [-n NUM_SENSORS]"
    echo "Options:"
    echo "  -v                      Enable verbose output."
    echo "  -i INITIAL_SENSOR_ADDR  Set the initial sensor address (default is 14)."
    echo "  -n NUM_SENSORS          Set the number of sensors to read (default is 123)."
    echo "  -h                      Display this help message."
}

while getopts "hvi:n:" opt; do
    case $opt in
        v)
            VERBOSE=1
            ;;
        i)
            INITIAL_SENSOR_ADDR=$OPTARG
            ;;
        n)
            NUM_SENSORS=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1
            ;;
    esac
done

fetch_snmp_walk_data() {
    local oid=$1
    local cmd="snmpwalk -v $SNMP_VERSION -c $SNMP_COMMUNITY $SNMP_HOST $oid"

    local output=$($cmd 2>&1)

    if [ $VERBOSE -eq 1 ]; then
        echo -e "\nSNMP Request: $cmd" >&2
        echo -e "Output:\n$output" >&2
    fi

    echo "$output"
}

parse_snmp_output() {
    local data=$1
    local arr_name=$2

    while IFS= read -r line; do
        if [[ $line =~ .*\.([0-9]+)\ =\ STRING\:\ \"(.*)\" ]]; then
            local index=${BASH_REMATCH[1]}
            local value=${BASH_REMATCH[2]}
            eval "$arr_name[$index]=\"$value\""
        elif [[ $line =~ .*\.([0-9]+)\ =\ INTEGER\:\ (.*) ]]; then
            local index=${BASH_REMATCH[1]}
            local value=${BASH_REMATCH[2]}
            eval "$arr_name[$index]=$value"
        fi
    done <<< "$data"
}

echo "Fetching sensor names..."
sensor_names_output=$(fetch_snmp_walk_data "$ROOT_OID.3.1.43.$IPMB_ADDR")

echo "Fetching sensor readings..."
sensor_readings_output=$(fetch_snmp_walk_data "$ROOT_OID.3.1.28.$IPMB_ADDR")

echo "Fetching sensor units..."
sensor_units_output=$(fetch_snmp_walk_data "$ROOT_OID.3.1.17.$IPMB_ADDR")

declare -A sensor_names sensor_readings sensor_units

parse_snmp_output "$sensor_names_output" sensor_names
parse_snmp_output "$sensor_readings_output" sensor_readings
parse_snmp_output "$sensor_units_output" sensor_units

for ((i=$INITIAL_SENSOR_ADDR; i<INITIAL_SENSOR_ADDR+NUM_SENSORS; i++)); do
    sensor_name=${sensor_names[$i]}
    sensor_reading=${sensor_readings[$i]}
    sensor_unit_index=${sensor_units[$i]}

    if [ "$sensor_unit_index" == "1" ]; then
        sensor_unit="degrees C" # Celsius for temperature
    else
        sensor_unit="unknown"
    fi

    echo "$sensor_name | $sensor_reading | $sensor_unit"
done